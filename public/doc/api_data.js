define({ "api": [
  {
    "type": "get",
    "url": "/",
    "title": "Поиск координат по наименованию",
    "name": "GetPosition",
    "description": "<p>Данный метод позволяет найти координаты по имени</p>",
    "group": "GroupService",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "query",
            "description": "<p><code>Обязательно</code> Адрес</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "HTTP",
          "content": "GET /?query=г Казань, ул 10 лет Октября, д 1 HTTP/1.1\nHost: http://germes.jlabs.pro",
          "type": "json"
        },
        {
          "title": "XHR",
          "content": "var data = null;\nvar xhr = new XMLHttpRequest();\nxhr.withCredentials = true;\n\nxhr.addEventListener(\"readystatechange\", function () {\n     if (this.readyState === 4) {\n          console.log(this.responseText);\n     }\n});\n\nxhr.open(\"GET\", \"http://germes.jlabs.pro/?query=г Казань, ул 10 лет Октября, д 1\");\n\nxhr.send(data);",
          "type": "javascript"
        },
        {
          "title": "PHP CURL",
          "content": "$curl = curl_init();\n\ncurl_setopt_array($curl, array(\n     CURLOPT_URL => \"http://germes.jlabs.pro/?query=г Казань, ул 10 лет Октября, д 1\",\n     CURLOPT_RETURNTRANSFER => true,\n     CURLOPT_ENCODING => \"\",\n     CURLOPT_MAXREDIRS => 10,\n     CURLOPT_TIMEOUT => 30,\n     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,\n     CURLOPT_CUSTOMREQUEST => \"GET\"\n));\n\n$response = curl_exec($curl);\n$err = curl_error($curl);\n\ncurl_close($curl);\n\nif ($err) {\n     echo \"cURL Error #:\" . $err;\n} else {\n     echo $response;\n}",
          "type": "php"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>0 - Ошибка, 1 - Success</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Текст ошибки  (только если status = 0)</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Информация по адресу (только если status = 1)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.system",
            "description": "<p>Система поиска</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.find",
            "description": "<p>Адрес</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.lat",
            "description": "<p>Широта</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "JSON удачный ответ:",
          "content": "HTTP/1.1 200 OK\n{\n     \"status\": 1,\n     \"error\": null,\n     \"data\": {\n         \"system\": \"dadata\",\n         \"find\": \"г Казань, ул 10 лет Октября, д 1\",\n         \"lat\": \"55.7007204\",\n         \"lng\": \"49.0885347\"\n     }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/routes.php",
    "groupTitle": "Методы"
  }
] });
