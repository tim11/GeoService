define({
  "name": "GeoService",
  "version": "1.0.0",
  "description": "Поиск координат по запросу",
  "url": "http://germes.jlabs.pro",
  "order": [
    "GetPosition"
  ],
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-05-16T05:01:30.671Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
