<?php

use GuzzleHttp\Client;

class Dadata
{
    public function getCoordinates($name){
        $response = [];
        if (file_exists(__DIR__ . "/../data/dadata.json")){
            try{
                $file = json_decode(file_get_contents(__DIR__ . "/../data/dadata.json"), false);
                if (!empty($file)){
                    foreach ($file as $key){
                        $http = new Client([
                            "base_uri" => "https://suggestions.dadata.ru"
                        ]);
                        $response = $http->request("get", "/suggestions/api/4_1/rs/suggest/address", [
                            "query" => [
                                "query" => $name,
                                "count" => 1
                            ],
                            "headers" => [
                                "Content-Type" => "application/json",
                                "Accept" => "application/json",
                                "Authorization" => "Token " . $key
                            ]
                        ]);
                        $code = $response->getStatusCode();
                        if ($code > 400 && $code < 404){
                            continue;
                        }elseif ($code == 200){
                            $body = $response->getBody();
                            $json = json_decode($body->getContents(), false);
                            $data = $json->suggestions[0]->data;
                            if (!empty($data->geo_lat) && !empty($data->geo_lon)){
                                $response = [
                                    "system" => "dadata",
                                    "find" => $json->suggestions[0]->value,
                                    "lat" => $data->geo_lat,
                                    "lng" => $data->geo_lon
                                ];
                            }
                        }
                    }
                }
            }catch (\Exception $e){

            }
        }
        return $response;
    }
}