<?php

use GuzzleHttp\Client;

/**
 * Created by PhpStorm.
 * User: TimJ
 * Date: 15.05.2018
 * Time: 23:58
 */

class Yandex
{
    public function getCoordinates($name){
        $response = [];
        try{
            $http = new Client([
                "base_uri" => "https://geocode-maps.yandex.ru"
            ]);
            $response = $http->request("get", "/1.x/", [
                "query" => [
                    "geocode" => $name,
                    "kind" => "house",
                    "format" => "json",
                    "lang" => "ru_RU",
                    "sco" => "longlat"
                ]
            ]);
            $code = $response->getStatusCode();
            if ($code == 200){
                $body = $response->getBody();
                $json = json_decode($body->getContents(), false);
                $data = $json->response->GeoObjectCollection->featureMember[0]->GeoObject;
                if (!empty($data->Point->pos)){
                    $latlng = explode(" ", $data->Point->pos);
                    $response = [
                        "system" => "yandex",
                        "find" => $data->metaDataProperty->GeocoderMetaData->text,
                        "lat" => $latlng[0],
                        "lng" => $latlng[1]
                    ];
                }
            }
        }catch (\Exception $e){

        }
        return $response;
    }
}