<?php
/**
 *
 * @apiDefine GroupService Методы
 *
 */
use Slim\Http\Request;
use Slim\Http\Response;

defined("CACHE_DIR") or define("CACHE_DIR", __DIR__ . "/../cache/");

/**
 * Поиск адреса по наименованию
 * @api {get} / Поиск координат по наименованию
 * @apiName GetPosition
 * @apiDescription Данный метод позволяет найти координаты по имени
 * @apiGroup GroupService
 *
 * @apiVersion 1.0.0
 *
 * @apiParam {String} query ```Обязательно``` Адрес
 *
 * @apiSuccess {Number} status 0 - Ошибка, 1 - Success
 * @apiSuccess {String} error Текст ошибки  (только если status = 0)
 * @apiSuccess {Object[]} data Информация по адресу (только если status = 1)
 * @apiSuccess {String} data.system Система поиска
 * @apiSuccess {String} data.find Адрес
 * @apiSuccess {Number} data.lat Широта
 * @apiSuccess {Number} data.lat Долгота
 *
 * @apiParamExample {json} HTTP
 * GET /?query=г Казань, ул 10 лет Октября, д 1 HTTP/1.1
 * Host: http://germes.jlabs.pro
 *
 * @apiParamExample {javascript} XHR
 * var data = null;
 * var xhr = new XMLHttpRequest();
 * xhr.withCredentials = true;
 *
 * xhr.addEventListener("readystatechange", function () {
 *      if (this.readyState === 4) {
 *           console.log(this.responseText);
 *      }
 * });
 *
 * xhr.open("GET", "http://germes.jlabs.pro/?query=г Казань, ул 10 лет Октября, д 1");
 *
 * xhr.send(data);

 * @apiParamExample {php} PHP CURL
 * $curl = curl_init();
 *
 * curl_setopt_array($curl, array(
 *      CURLOPT_URL => "http://germes.jlabs.pro/?query=г Казань, ул 10 лет Октября, д 1",
 *      CURLOPT_RETURNTRANSFER => true,
 *      CURLOPT_ENCODING => "",
 *      CURLOPT_MAXREDIRS => 10,
 *      CURLOPT_TIMEOUT => 30,
 *      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
 *      CURLOPT_CUSTOMREQUEST => "GET"
 * ));
 *
 * $response = curl_exec($curl);
 * $err = curl_error($curl);
 *
 * curl_close($curl);
 *
 * if ($err) {
 *      echo "cURL Error #:" . $err;
 * } else {
 *      echo $response;
 * }
 *
 * @apiSuccessExample {json} JSON удачный ответ:
 * HTTP/1.1 200 OK
 * {
 *      "status": 1,
 *      "error": null,
 *      "data": {
 *          "system": "dadata",
 *          "find": "г Казань, ул 10 лет Октября, д 1",
 *          "lat": "55.7007204",
 *          "lng": "49.0885347"
 *      }
 * }
 */
$app->get('/', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");
    $query = $request->getQueryParam("query", null);
    if (empty($query)){
        return $response->withJson([
            "status" => 0,
            "error" => "Укажите параметр запроса"
        ], 400);
    }
    if (!file_exists(CACHE_DIR) || !is_dir(CACHE_DIR)){
        mkdir(CACHE_DIR, 0755, true);
    }
    $cacheFile = CACHE_DIR . md5($query);
    if (file_exists($cacheFile) && (filectime($cacheFile) + (24 * 3600)) > time()){
        $data = json_decode(file_get_contents($cacheFile));
        $use_cache = true;
    }else{
        $use_cache = false;
        $data= $this->dadata->getCoordinates($query);
        if (empty($data)){
            $data = $this->yandex->getCoordinates($query);
            if (empty($data)){
                return $response->withJson([
                    "status" => 0,
                    "error" => "Ничего не найдено"
                ], 404);
            }
        }
    }
    if (!$use_cache){
        file_put_contents($cacheFile, json_encode($data));
    }

    return $response->withJson([
        "status" => 1,
        "error" => null,
        "data" => $data
    ]);
});
